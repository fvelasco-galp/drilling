
import pandas as pd
import os

#import nltk
#import numpy as np
#import sklearn.metrics
#from sklearn.model_selection import train_test_split
#from sklearn.metrics import classification_report
#from sklearn.feature_extraction.text import TfidfVectorizer
#from time import time
#from polyglot.detect import Detector
#import icu

pp = __import__('drilling_preprocessing')

# Se crea un único dataframe con la info de todos los ficheros
pathTrainingFiles = 'files/train/'
df = pd.DataFrame(columns=['NPT Cause', 'NPT Description', 'Phase', 'Operations', 'Sub-Operations', 'Description'])

# Se leen todos los Excel y se concatenan en un único dataframe
for filename in os.listdir(pathTrainingFiles):
    df_excel = pd.read_excel(pathTrainingFiles + filename, sheet_name='Time Log')
    df_excel = df_excel[['NPT Cause', 'NPT Description', 'Phase', 'Operations', 'Sub-Operations', 'Description']]
    df_excel = df_excel[:df_excel['Description'].count()]
    df = pd.concat([df, df_excel], ignore_index=True, sort=False)

print(str(len(os.listdir(pathTrainingFiles))) + " ficheros leídos.")
print("O primer elemento es:\n" + str(df.loc[0, :]))


print("Total: " + str(len(df['Description'])) + " registros - Description.")

# TODO: Clasificación con diferentes categorías
#ola