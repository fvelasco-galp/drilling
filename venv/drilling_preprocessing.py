
import pandas as pd
import numpy as np
import nltk
from nltk.stem import RSLPStemmer
from sklearn.feature_extraction.text import TfidfVectorizer


def lowerCase(df):
    total_rows = len(df.index)
    n = 0

    while n < total_rows:
        df.iat[n] = (df.iat[n]).lower()
        n = n + 1

    return (df)


def removePunctuation(df):
    punct = ['!', '#', '$', '%', '&', "'", '(', ')', '*', '+', ',', '-', ':', ';', '<', '=', '>', '?', '@', '[', '\\',
             ']', '^', '_', '`', '{', '|', '}', '~']

    p = 0
    total_rows = len(df.index)

    while p < total_rows:
        phrase = list(df.iat[p])
        for element in phrase:
            if element in punct:
                phrase.remove(element)
                df.loc[p] = ''.join(phrase)  # junta em STRING - em LIST - df.loc[p] = phrase
        p = p + 1

    return df


def stemming(df):
    total_rows = len(df.index)
    idx = np.r_[0:total_rows]

    n = 0
    stemmer = RSLPStemmer()
    df_stem = pd.Series(idx)
    phrase = []

    while n < total_rows:
        sentence = df.iat[n]
        for word in sentence:
            phrase.append(stemmer.stem(word))

        df_stem.loc[n] = ' '.join(phrase)  # junta em STRING - em LIST - df.loc[p] = phrase
        phrase = []
        n = n + 1

    return df_stem


def removeStopwords(df):
    stop_w = list(nltk.corpus.stopwords.words("portuguese"))

    p = 0
    total_rows = len(df.index)

    while p < total_rows:
        phrase = list(df.iat[p].split())
        for word in phrase:
            if word in stop_w:
                phrase.remove(word)
                df.loc[p] = ' '.join(phrase)  # junta em STRING - em LIST - df.loc[p] = phrase
        p = p + 1

    return df


def setTFIDF(corpus):
    X = [[]]
    vectorizer = TfidfVectorizer()
    X = np.array(vectorizer.fit_transform(corpus))

    return X


def tokenize(df):
    total_rows = len(df.index)

    n = 0

    while n < total_rows:
        phrase = df.iat[n]
        tokens = nltk.word_tokenize(phrase)
        df.iat[n] = tokens
        n = n + 1

    return df


def replaced(sequence, old, new):
    return (new if x == old else x for x in sequence)


def removeAccents(df):
    a_accents = ['á', 'â', 'à', 'ã']
    e_accents = ['é', 'ê']
    i_accents = ['í']
    o_accents = ['ó', 'ô', 'õ']
    u_accents = ['ú']
    c_accents = ['ç']

    total_rows = len(df.index)

    # Letter A

    n = 0
    p = 0
    while p < total_rows:
        words = list(df.iat[p])
        for element in words:
            if element in a_accents:
                n = words.index(element)
                words[n] = 'a'
                df.loc[p] = ''.join(words)  # junta em STRING - em LIST - df.loc[p] = words
            n = n + 1
        p = p + 1

    # Letter E

    n = 0
    p = 0
    while p < total_rows:
        words = list(df.iat[p])
        for element in words:
            if element in e_accents:
                n = words.index(element)
                words[n] = 'e'
                df.loc[p] = ''.join(words)  # junta em STRING - em LIST - df.loc[p] = words
            n = n + 1
        p = p + 1

    # Letter I

    n = 0
    p = 0
    while p < total_rows:
        words = list(df.iat[p])
        for element in words:
            if element in i_accents:
                n = words.index(element)
                words[n] = 'i'
                df.loc[p] = ''.join(words)  # junta em STRING - em LIST - df.loc[p] = words
            n = n + 1
        p = p + 1

    # Letter O

    n = 0
    p = 0
    while p < total_rows:
        words = list(df.iat[p])
        for element in words:
            if element in o_accents:
                n = words.index(element)
                words[n] = 'o'
                df.loc[p] = ''.join(words)  # junta em STRING - em LIST - df.loc[p] = words
            n = n + 1
        p = p + 1

        # Letter U

    n = 0
    p = 0
    while p < total_rows:
        words = list(df.iat[p])
        for element in words:
            if element in u_accents:
                n = words.index(element)
                words[n] = 'u'
                df.loc[p] = ''.join(words)  # junta em STRING - em LIST - df.loc[p] = words
            n = n + 1
        p = p + 1

    # Letter C

    n = 0
    p = 0
    while p < total_rows:
        words = list(df.iat[p])
        for element in words:
            if element in c_accents:
                n = words.index(element)
                words[n] = 'c'
                df.loc[p] = ''.join(words)  # junta em STRING - em LIST - df.loc[p] = words
            n = n + 1
        p = p + 1

    return df
