
import nltk
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
import sklearn.metrics
from pandas import DataFrame,Series
from sklearn.feature_extraction.text import TfidfVectorizer
from time import time
import matplotlib.pyplot as plt
from polyglot.detect import Detector
import icu


def detect_lang(df):
    # list of comments and sentiment score
    comment_lst = list()

    for idx, row in df.iterrows():
        detector = Detector(row['COMMENT'], quiet=True)
        comment_lst.append([row['ID'], row['COMMENT'], row['SENTIMENT_TYPE'], detector.language.code])

    return pd.DataFrame(comment_lst, columns=['ID', 'COMMENT', 'CATEGORY', 'LANG'])


# read CSV data
# File to process
nps_file = 'nps_comments_classification.csv'
path = 'files/' + nps_file

# read file
nps_data_df = pd.read_csv(path, sep=';')

df = detect_lang(nps_data_df)

df = df[(df.CATEGORY.isin(['Compliment', 'Depreciation', 'Proposal'])) & (df.LANG == 'en')]

print("The data-set has %d rows and %d columns"%(df.shape[0],df.shape[1])) #using the shape attribute of the dataframe object.
#where the first element shows the number of rows and the second element shows the number of columns.
df.describe(include='all')

category_counter={x:0 for x in set(df['CATEGORY'])}
for each_cat in df['CATEGORY']:
    category_counter[each_cat]+=1

print(category_counter)

corpus=df.COMMENT
all_words=[w.split() for w in corpus]

all_flat_words=[ewords for words in all_words for ewords in words]

from nltk.corpus import stopwords

#removing all the stop words from the corpus
all_flat_words_ns=[w for w in all_flat_words if w not in stopwords.words("english")]

#removing all duplicates
#I didn't convert my text into unicode and that's why this subtle warning message appears. by using unicode() function, this
#warning can easily be addressed.
set_nf=set(all_flat_words_ns)
print("Number of unique vocabulary words in the text_description column of the dataframe: %d"%len(set_nf))